FROM python:3.9

WORKDIR hispawars

COPY . /hispawars

RUN apt-get upgrade -y
RUN pip install -e hispawars_game
RUN pip install -e hispawars_bot
RUN pip install -e hispawars_viz_aichallenge
RUN pip install -r requirements.txt
RUN chmod u+x play.sh

ENTRYPOINT ["./play.sh"]
