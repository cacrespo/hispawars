# Hispawars

Bot battle competition based on the [AI Challenge](http://ants.aichallenge.org/).

To set up a development environment and run a game, you need to install the 3 hispawars packages:

- `pip install -e hispawars_game`
- `pip install -e hispawars_bot`
- `pip install -e hispawars_viz_aichallenge`
- `./play.sh` (or `play` in Windows)
